#!/bin/sh

# File: avc-service-status-core-it/.gitlab-ci/wait_for_docker_containers.sh

docker ps | tail -n +2 | awk '{print $1}' | tac | while read i; do

    echo "${i}"

    LOOP=xxxxxx # Wait for 6 x 5 seconds

    while [ -n "${LOOP}" ]; do

    	date

	    if docker logs "${i}" | grep -q "database system is ready to accept connections"; then

        	LOOP=

        else

	    	sleep 5;

    		LOOP=`echo $LOOP | cut -c 2-`

    	fi

    done

    docker logs "${i}"

done
