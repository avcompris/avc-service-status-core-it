package net.avcompris.status.core.it.RDS;

import static net.avcompris.commons3.core.it.utils.RDSTestUtils.ensureDbTableName;
import static net.avcompris.commons3.core.it.utils.RDSTestUtils.getDataSource;
import static net.avcompris.status.dao.impl.DbTables.SERVICES;

import java.util.stream.Stream;

import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.provider.Arguments;

import net.avcompris.commons3.api.tests.ApiTestsUtils;
import net.avcompris.commons3.api.tests.TestSpecParametersExtension;
import net.avcompris.commons3.api.tests.TestsSpec.TestSpec;
import net.avcompris.commons3.utils.Clock;
import net.avcompris.commons3.web.HealthCheck;
import net.avcompris.status.api.ServiceStatus;
import net.avcompris.status.api.ServiceStatusHistory;
import net.avcompris.status.api.ServicesStatus;
import net.avcompris.status.api.ServicesStatusHistory;
import net.avcompris.status.api.StatusConfig;
import net.avcompris.status.core.tests.AbstractStatusServiceApiTest;
import net.avcompris.status.dao.StatusDao;
import net.avcompris.status.dao.impl.StatusDaoInRDS;

@ExtendWith(TestSpecParametersExtension.class)
public class StatusServiceApiTests extends AbstractStatusServiceApiTest {

	public StatusServiceApiTests(final TestSpec spec) {

		super(spec);
	}

	@Override
	protected StatusDao getBeans(final Clock clock) throws Exception {

		final String servicesTableName = ensureDbTableName(SERVICES);

		final StatusDao statusDao = new StatusDaoInRDS(getDataSource(), servicesTableName, clock);

		return statusDao;
	}

	public static Stream<Arguments> testSpecs() throws Exception {

		return ApiTestsUtils.testSpecs( //
				HealthCheck.class, //
				StatusConfig.class, //
				ServiceStatus.class, //
				ServicesStatus.class, //
				ServiceStatusHistory.class, //
				ServicesStatusHistory.class);
	}
}
