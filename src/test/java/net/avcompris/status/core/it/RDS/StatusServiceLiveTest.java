package net.avcompris.status.core.it.RDS;

import static net.avcompris.commons3.core.it.utils.RDSTestUtils.ensureDbTableName;
import static net.avcompris.commons3.core.it.utils.RDSTestUtils.getDataSource;
import static net.avcompris.status.dao.impl.DbTables.SERVICES;

import net.avcompris.commons3.utils.Clock;
import net.avcompris.status.core.it.AbstractStatusServiceLiveTest;
import net.avcompris.status.dao.StatusDao;
import net.avcompris.status.dao.impl.StatusDaoInRDS;

public class StatusServiceLiveTest extends AbstractStatusServiceLiveTest {

	@Override
	protected StatusDao getBeans(final Clock clock) throws Exception {

		final String servicesTableName = ensureDbTableName(SERVICES);

		final StatusDao statusDao = new StatusDaoInRDS(getDataSource(), servicesTableName, clock);

		return statusDao;
	}
}