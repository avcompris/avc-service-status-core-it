package net.avcompris.status.core.it;

import static com.google.common.io.Resources.getResource;
import static com.google.common.io.Resources.toByteArray;
import static net.avcompris.commons3.core.tests.CoreTestUtils.defaultClock;
import static net.avcompris.commons3.core.tests.CoreTestUtils.newCorrelationId;
import static net.avcompris.commons3.databeans.DataBeans.instantiate;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNotSame;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertSame;

import java.io.File;

import org.apache.commons.io.FileUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import net.avcompris.commons3.core.Permissions;
import net.avcompris.commons3.core.impl.PermissionsImpl;
import net.avcompris.commons3.core.tests.AbstractServiceTest;
import net.avcompris.commons3.utils.LogFactory;
import net.avcompris.status.api.InlineService;
import net.avcompris.status.api.ServiceStatus;
import net.avcompris.status.api.ServiceStatusHistory;
import net.avcompris.status.api.ServicesStatus;
import net.avcompris.status.api.ServicesStatusHistory;
import net.avcompris.status.api.StatusService;
import net.avcompris.status.api.TriggerType;
import net.avcompris.status.core.impl.StatusServiceImpl;
import net.avcompris.status.dao.StatusDao;
import net.avcompris.status.query.CheckStatus;

public abstract class AbstractStatusServiceLiveTest extends AbstractServiceTest<StatusDao> {

	protected StatusService statusService;

	@BeforeEach
	public final void setUpBeans() throws Exception {

		final StatusDao statusDao = getBeans(defaultClock());

		final Permissions permissions = new PermissionsImpl();

		System.setProperty("configFile", "target/service_status.yml");

		FileUtils.writeByteArrayToFile(new File("target", "service_status.yml"),
				toByteArray(getResource("service_status.yml")));

		statusService = new StatusServiceImpl(permissions, defaultClock(), statusDao);

		LogFactory.resetCorrelationId();
	}

	@Test
	public final void testGetLive_getHistory() throws Exception {

		final String correlationId = newCorrelationId();

		final ServicesStatusHistory history0 = statusService.getServicesStatusHistory(correlationId);

		assertNotNull(history0);

		assertEquals(1, history0.getItems().length);

		final ServiceStatusHistory item0_0 = history0.getItems()[0];

		assertEquals("google", item0_0.getServiceId());
		assertEquals("GET https://www.google.com/", item0_0.getEndpoint());

		final int count0 = item0_0.getTotal();

		statusService.getServicesLiveStatus(correlationId);

		final ServicesStatusHistory history1 = statusService.getServicesStatusHistory(correlationId);

		assertNotNull(history1);

		assertEquals(1, history1.getItems().length);

		final ServiceStatusHistory item1_0 = history1.getItems()[0];

		assertEquals("google", item1_0.getServiceId());
		assertEquals("GET https://www.google.com/", item1_0.getEndpoint());

		final int count1 = item1_0.getTotal();

		assertEquals(count0 + 1, count1);

		assertNotNull(item1_0.getChecks()[0].getId());
		assertNotNull(item1_0.getChecks()[0].getStartedAt());
		assertEquals(true, item1_0.getChecks()[0].isSuccess());
		assertSame(CheckStatus.SUCCESS, item1_0.getChecks()[0].getStatus());
		assertNotNull(item1_0.getChecks()[0].getEndedAt());
		assertNotNull(item1_0.getChecks()[0].getElapsedMs());
		assertNull(item1_0.getChecks()[0].getErrorMessage());
		assertEquals(200, item1_0.getChecks()[0].getStatusCode().intValue());
		assertNotSame(TriggerType.CRON, item1_0.getChecks()[0].getTriggerType());
	}

	@Test
	public final void testGetLive() throws Exception {

		final String correlationId = newCorrelationId();

		final ServicesStatus status = statusService.getServicesLiveStatus(correlationId);

		assertEquals(1, status.getItems().length);

		final ServiceStatus item0 = status.getItems()[0];

		assertEquals("google", item0.getServiceId());
		assertEquals("GET https://www.google.com/", item0.getEndpoint());

		assertNotNull(item0.getCheck().getId());
		assertNotNull(item0.getCheck().getStartedAt());
		assertEquals(true, item0.getCheck().isSuccess());
		assertSame(CheckStatus.SUCCESS, item0.getCheck().getStatus());
		assertNotNull(item0.getCheck().getEndedAt());
		assertNotNull(item0.getCheck().getElapsedMs());
		assertNull(item0.getCheck().getErrorMessage());
		assertEquals(200, item0.getCheck().getStatusCode().intValue());
		assertNotSame(TriggerType.CRON, item0.getCheck().getTriggerType());
	}

	@Test
	public final void testGetLive_oneService() throws Exception {

		final String correlationId = newCorrelationId();

		final ServiceStatus status = statusService.getServiceLiveStatus(correlationId, "google");

		assertEquals("google", status.getServiceId());
		assertEquals("GET https://www.google.com/", status.getEndpoint());

		assertNotNull(status.getCheck().getId());
		assertNotNull(status.getCheck().getStartedAt());
		assertEquals(true, status.getCheck().isSuccess());
		assertSame(CheckStatus.SUCCESS, status.getCheck().getStatus());
		assertNotNull(status.getCheck().getEndedAt());
		assertNotNull(status.getCheck().getElapsedMs());
		assertNull(status.getCheck().getErrorMessage());
		assertEquals(200, status.getCheck().getStatusCode().intValue());
		assertNotSame(TriggerType.CRON, status.getCheck().getTriggerType());
	}

	@Test
	public final void testGetLive_Inline() throws Exception {

		final String correlationId = newCorrelationId();

		final ServiceStatus status = statusService.getInlineServiceLiveStatus(correlationId, "xxx",
				instantiate(InlineService.class) //
						.setEndpoint("GET https://www.apple.com/") //
						.setExpect(instantiate(InlineService.Expect.class) //
								.setStatusCode(200)));

		assertEquals("xxx", status.getServiceId());
		assertEquals("GET https://www.apple.com/", status.getEndpoint());

		assertNotNull(status.getCheck().getId());
		assertNotNull(status.getCheck().getStartedAt());
		assertEquals(true, status.getCheck().isSuccess());
		assertSame(CheckStatus.SUCCESS, status.getCheck().getStatus());
		assertNotNull(status.getCheck().getEndedAt());
		assertNotNull(status.getCheck().getElapsedMs());
		assertNull(status.getCheck().getErrorMessage());
		assertEquals(200, status.getCheck().getStatusCode().intValue());
		assertNotSame(TriggerType.CRON, status.getCheck().getTriggerType());
	}

	@Test
	public final void testGetLive_Inline_defaultExpect() throws Exception {

		final String correlationId = newCorrelationId();

		final ServiceStatus status = statusService.getInlineServiceLiveStatus(correlationId, "xxx",
				instantiate(InlineService.class) //
						.setEndpoint("GET https://www.apple.com/"));

		assertEquals("xxx", status.getServiceId());
		assertEquals("GET https://www.apple.com/", status.getEndpoint());

		assertNotNull(status.getCheck().getId());
		assertNotNull(status.getCheck().getStartedAt());
		assertEquals(true, status.getCheck().isSuccess());
		assertSame(CheckStatus.SUCCESS, status.getCheck().getStatus());
		assertNotNull(status.getCheck().getEndedAt());
		assertNotNull(status.getCheck().getElapsedMs());
		assertNull(status.getCheck().getErrorMessage());
		assertEquals(200, status.getCheck().getStatusCode().intValue());
		assertNotSame(TriggerType.CRON, status.getCheck().getTriggerType());
	}

	@Test
	public final void testGetLive_Inline_MalformedEndpoint() throws Exception {

		final String correlationId = newCorrelationId();

		final ServiceStatus status = statusService.getInlineServiceLiveStatus(correlationId, "xxx",
				instantiate(InlineService.class) //
						.setEndpoint("https://www.apple.com/"));

		assertNotNull(status.getCheck().getId());
		assertNotNull(status.getCheck().getStartedAt());
		assertEquals(false, status.getCheck().isSuccess());
		assertSame(CheckStatus.ERROR, status.getCheck().getStatus());
		assertNotNull(status.getCheck().getEndedAt());
		assertNull(status.getCheck().getElapsedMs());
		assertNotNull(status.getCheck().getErrorMessage());
		assertEquals("Endpoint should start with \"GET \", but was: https://www.apple.com/",
				status.getCheck().getErrorMessage());
		assertNull(status.getCheck().getStatusCode());
		assertNotSame(TriggerType.CRON, status.getCheck().getTriggerType());
	}

	@Test
	public final void testGetLive_Inline_http() throws Exception {

		final String correlationId = newCorrelationId();

		final ServiceStatus status = statusService.getInlineServiceLiveStatus(correlationId, "xxx",
				instantiate(InlineService.class) //
						.setEndpoint("GET http://apple.com/") //
						.setExpect(instantiate(InlineService.Expect.class) //
								.setStatusCode(301)));

		assertEquals("xxx", status.getServiceId());
		assertEquals("GET http://apple.com/", status.getEndpoint());

		assertNotNull(status.getCheck().getId());
		assertNotNull(status.getCheck().getStartedAt());
		assertEquals(true, status.getCheck().isSuccess());
		assertSame(CheckStatus.SUCCESS, status.getCheck().getStatus());
		assertNotNull(status.getCheck().getEndedAt());
		assertNotNull(status.getCheck().getElapsedMs());
		assertNull(status.getCheck().getErrorMessage());
		assertEquals(301, status.getCheck().getStatusCode().intValue());
		assertNotSame(TriggerType.CRON, status.getCheck().getTriggerType());
	}

	@Test
	public final void testGetLive_Inline_wrongStatusCode() throws Exception {

		final String correlationId = newCorrelationId();

		final ServiceStatus status = statusService.getInlineServiceLiveStatus(correlationId, "xxx",
				instantiate(InlineService.class) //
						.setEndpoint("GET https://www.apple.com/") //
						.setExpect(instantiate(InlineService.Expect.class) //
								.setStatusCode(404)));

		assertEquals("xxx", status.getServiceId());
		assertEquals("GET https://www.apple.com/", status.getEndpoint());

		assertNotNull(status.getCheck().getId());
		assertNotNull(status.getCheck().getStartedAt());
		assertEquals(false, status.getCheck().isSuccess());
		assertSame(CheckStatus.ERROR, status.getCheck().getStatus());
		assertNotNull(status.getCheck().getEndedAt());
		assertNotNull(status.getCheck().getElapsedMs());
		assertNotNull(status.getCheck().getErrorMessage());
		assertEquals(200, status.getCheck().getStatusCode().intValue());
		assertNotSame(TriggerType.CRON, status.getCheck().getTriggerType());
	}
}